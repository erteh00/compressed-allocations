const MS_PER_DAY = 86400 * 1000;

/** 2021-07-15T04:49:00Z in epoch-milliseconds */
let SACRIFICE_DAY0_TIMESTAMP_MS = 0;

export const setSacrificeDay0 = (d: Date) => {
  SACRIFICE_DAY0_TIMESTAMP_MS = +d;
};

export const getAuditDate = (ts: number): Date => (
  new Date(Math.max(SACRIFICE_DAY0_TIMESTAMP_MS, ts))
);

/** Day number since the start of the sacrifice period (0-based) */
export type SacrificeDay = number;

const CREDITS_PER_USD_BASE = 10000n;

export const enum BonusType {
  /** For latecomers after the end of the sacrifice period */
  NONE,

  /** A fixed 75% bonus for the last 3 days of the sacrifice period */
  FLAT,

  /** For the first 16 days of the sacrifice period.
    * Must be aggregated by address so that a total volume curve can be calculated */
  VOLUME,
}

/**
 * The rules for sacrifice phase were:
 *   - the first 16 days would be bonused on a "volume curve"
 *   - with the last 3 days getting a "flat" bonus
 *   - given the immense participation after the "end" of the sac phase,
 *     late sacrifices are allowed but are not bonused
 * @param {SacrificeDay} day day number since the start of the sacrifice period
 * @returns VOLUME, FLAT, or NONE
 */
export function getBonusType(volumeCutoff: number, flatCutoff: number, day: SacrificeDay): BonusType {
  if (day < volumeCutoff) { // Days 0..15
    return BonusType.VOLUME;
  }
  if (day < flatCutoff) { // Days 16..18
    return BonusType.FLAT;
  }
  // Days 19..n
  return BonusType.NONE;
}

/**
 * Calculate the 75% flat bonus for a sacrifice
 * @param credits base number of points before bonuses
 * @returns the additional points from the flat bonus
 */
export function calcFlatBonus(credits: bigint): bigint {
  return (credits * 75n) / 100n;
}

/**
 * Calculate the volume bonus for a sacrifice, accounting for the volume of sacrifices
 * smaller than it, and the total volume of all sacrifices.
 * @param cumulativeVolumeBelow (V₀) the total number of points "smaller" than this block \
 *            e.g. for sacrifices of [3, 7, 8, 10, 13], and
 *                 calculating the bonus for the sacrifice of 10, then V₀ = 18
 *
 * @param volume (V) the volume of the sacrifice in question \
 *            e.g. for sacrifices of [3, 7, 8, 10, 13], and
 *                 calculating the bonus for the sacrifice of 10, then V = 10 (and V₀ = 18)
 *
 * @param totalVolume (T) the total of all sacrifices \
 *            e.g. for sacrifices of [3, 7, 8, 10, 13], then T = 41
 * @returns
 * ```
 *      3          1         2V₀ + V - 1        V         1
 * ­    ———   ×   —————   ×   ———————————   ×   ———   ×   ———
 *      2        T - 1            1             1         2
 * ```
 * Here's an example returning a list of pairs of sacrifice [volume, bonus]:
 * ```js
 * const orderedAscending = [3n, 7n, 8n, 10n, 13n];
 * const totalVolume = orderedAscending.reduce((t, v) => t + v, 0n);
 *
 * let cumulativeVolumeBelow = 0n;
 * const bonused = orderedAscending.map(volume => {
 *   const bonus = calcVolumeBonus(cumulativeVolumeBelow, volume, totalVolume);
 *   cumulativeVolumeBelow += volume;
 *   return [volume, bonus];
 * });
 * ```
 */
export function calcVolumeBonus(cumulativeVolumeBelow: bigint, volume: bigint, totalVolume: bigint): bigint {
  return (
    (3n * ((2n * cumulativeVolumeBelow) + volume - 1n) * volume)
      /
    (2n * (totalVolume - 1n) * 2n)
  );
}

/**
 *
 * @param {*} ts epoch time in milliseconds for the sacrifice
 * @returns the sacrifice day for "rate" calculation, with the minimum of day 0 (first day) for early SENS donators
 */
export function getSacrificeDay(ts: number): SacrificeDay {
  return Math.max(0, Math.floor((ts - SACRIFICE_DAY0_TIMESTAMP_MS) / MS_PER_DAY));
}

/**
 * @param {Number} priceIncreaseDay day that the price begins to increase by 5%
 * @param {Number} scaledUsdValue value of sacrifice in USD scaled to 18 decimals \
 *                                (typically the product of two 18-decimal numbers)
 * @param {SacrificeDay} day day number since the start of the sacrifice period (0-based)
 * @param {boolean} fromSens flag indicating this was a SENS donation credit
 * @returns the scaled sacrifice credits, accounting for the rate of conversion based on day
 */
export function calcSacrificeCredits(
  priceIncreaseDay: number,
  scaledUsdValue: bigint,
  day: SacrificeDay,
  fromSens: boolean,
): bigint {
  const creditsBase = fromSens
    ? scaledUsdValue * CREDITS_PER_USD_BASE * 75n / 100n
    : scaledUsdValue * CREDITS_PER_USD_BASE;

  let credits: bigint;
  if (day < priceIncreaseDay) {
    // Days 0..4
    // No price increase

    credits = creditsBase;
  } else {
    // Days 5..n
    // Increase price by 5% for each day, by dividing by 1.05^days
    const days = BigInt(day - priceIncreaseDay + 1);

    credits = (creditsBase * 100n**days) / (105n**days);
  }
  return credits;
}
