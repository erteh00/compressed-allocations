import * as path from 'path';
import * as fs from 'fs';

import Papa from 'papaparse';
import * as yargs from 'yargs';
import BigNumber from 'bignumber.js';

import { formatAuditDate, appendAudit, AuditEntry, mapAppend } from './audit';
import * as paths from './paths'
import {
  getSacrificeDay,
  calcSacrificeCredits,
  BonusType,
  getBonusType,
  calcFlatBonus,
  calcVolumeBonus,
  getAuditDate,
  setSacrificeDay0,
} from './bonus';
import {
  exportCreditsCsv,
  exportDistributionList,
  exportBonusAudit,
  exportRawReducedCsv,
} from './export-credits';
import { exportRawCsv } from './export-raw';
import { dedupSourcePrices, filterUndesirables, importRawCsv } from './import-raw';
import { Address, Sacrifice, SacrificeMap, Point } from './types';
import {
  DECIMALS_18_ONE,
} from './utils';

BigNumber.config({
  DECIMAL_PLACES: 100,
  EXPONENTIAL_AT: 101,
});

type PassedArgs = {
  project: string;
  startTime?: string;
  volumeCutoff?: number;
  flatCutoff?: number;
  priceIncreaseDay?: number;
};

type Args = {
  project: string;
  startTime: string;
  volumeCutoff: number;
  flatCutoff: number;
  priceIncreaseDay: number;
};

const argv = yargs.options({
  project: {
    type: 'string',
    describe: 'the subfolder to use as the input',
    require: true,
  },
  startTime: {
    type: 'string',
    describe: 'the point in time that is the zero date',
  },
  priceIncreaseDay: {
    type: 'number',
    describe: 'the day that the price for a single point will begin increasing by 5%',
  },
  volumeCutoff: {
    type: 'number',
    describe: 'the day after which the points are no longer increased in price',
  },
  flatCutoff: {
    type: 'number',
    describe: 'the day after which the points are no longer flat in price after the curve',
  },
});

const passedArgs: PassedArgs = argv.parseSync();
const comprehensiveAccountingAddress = '0xAd68E920fBFb5aAF4E5899b8104bf7cb22D31dEa';
const noExpectationsAddress = '0xfF1556DDD8a5204b19761de90a9Bd48586821284';
const ammbotAddress = '0x9B4e0C2f3D392E6DfA2A342132e5B8ED9457A5C9';

type Config = {
  startTime: string;
  priceIncreaseDay: number;
  volumeCutoff: number;
  flatCutoff: number;
};

const pulsechainDefaultArgs = {
  startTime: '2021-07-15T04:50:00.000Z',
  priceIncreaseDay: 5,
  volumeCutoff: 16,
  flatCutoff: 19,
};

const pulsexDefaultArgs = {
  startTime: '2021-12-29T00:00:00Z',
  priceIncreaseDay: 12,
  volumeCutoff: 53,
  flatCutoff: 59,
};

const configs: {
  [key: string]: Config;
} = {
  pulsechain: pulsechainDefaultArgs,
  pulsex: pulsexDefaultArgs,
};

const args: Args = Object.assign(configs[passedArgs.project], passedArgs);

const {
  project,
  volumeCutoff,
  flatCutoff,
  priceIncreaseDay,
  startTime,
} = args;
const logs: [string, string | number | Date][] = [
  ['project', project],
  ['start time', startTime],
  ['price increase', priceIncreaseDay],
  ['volume cutoff', volumeCutoff],
  ['flat cutoff', flatCutoff],
];
const longest = logs.reduce((memo, log) => (
  Math.max(memo, log[0].length)
), 0);
console.log(logs.map(([log, value]) => (
  [log.padStart(longest, ' '), value].join(' - ')
)).concat(['']).join('\n'));

const pathTo = paths.fromProject(project)

setSacrificeDay0(new Date(startTime));

main().catch(console.error);

function dollarValueOfSacrifices(sacrifices: Sacrifice[]): BigNumber {
  return sacrifices.reduce((memo, sac) => memo.plus(
    (new BigNumber(sac.amount.toString()))
      .dividedBy(`1e${sac.decimals}`)
      .times(sac.usdPrice.toString())
      .dividedBy(1e18),
  ), new BigNumber(0));
}

async function main() {
  const sacrifices = importRawCsv(pathTo.RAW_CSV_PATH);

  const { categories } = dedupSourcePrices(sacrifices);

  const keyToPath = new Map<string, string>([
    ['advertisedFor', pathTo.RAW_ADVERTISED_FOR_CSV_PATH],
    ['notIgnoring', pathTo.RAW_NOT_IGNORED_CSV_PATH],
    ['ignoring', pathTo.RAW_IGNORED_CSV_PATH],
    ['sales', pathTo.RAW_SALES_CSV_PATH],
  ]);
  const used = [
    'advertisedFor',
    // 'notIgnoring',
    // 'ignoring',
    'sales',
  ];
  // write out all files
  [...keyToPath.entries()].map(([key, filePath]) => (
    exportRawCsv(filePath, [...(categories.get(key) as SacrificeMap).values()])
  ));

  const accepted = used.reduce((memo, key) => (
    new Map<string, Sacrifice>([...memo, ...categories.get(key) as Map<string, Sacrifice>])
  ), new Map<string, Sacrifice>());
  const acceptedAsList = filterUndesirables([...accepted.values()]);
  const summaries = [...categories.keys()].map((key) => {
    const category = categories.get(key) as Map<string, Sacrifice>;
    return `${category.size} ${key} for ($${dollarValueOfSacrifices([...category.values()]).toString()})`;
  });
  console.info(`Imported ${sacrifices.length} Sacrifices
  ${summaries.join('\n  ')}

${accepted.size} accepted: ${used.join(' + ')}`);

  const {
    addrPoints,
    addrAudits: _,
    pointBreakdown,
    total,
  } = await allocatePoints(acceptedAsList);
  const pointTotal = [...pointBreakdown.entries()].reduce((memo, [_key, points]) => (
    points.reduce((mapping, point) => {
      const current = mapping.get(point.type) || 0n;
      mapping.set(point.type, current + (point.value || 0n));
      return mapping;
    }, memo)
  ), new Map<string, bigint>());
  console.log('points total\t', total);
  console.log('audit total\t', [...pointTotal.values()].reduce((memo, next) => memo + next));
  console.log('audit categories\t', pointTotal);
  exportBonusAudit(pathTo.POINT_EVENT_PATH, pointBreakdown);
  exportRawReducedCsv(pathTo.RAW_REDUCED_CSV_PATH, sacrifices);
  exportCreditsCsv(pathTo.CREDITS_CSV_PATH, addrPoints);
  if (project === 'pulsex') {
    exportDistributionList(pathTo.PULSEX_DISTRIBUTION_CSV_PATH, addrPoints)
  }
  console.info(`Exported ${addrPoints.size} Addresses`);
}

async function allocatePoints(sacrifices: Sacrifice[]) {
  const addrPoints = new Map<Address, bigint>();
  const addrAudits = new Map<Address, AuditEntry[]>();
  const pointBreakdown = new Map<Address, Point[]>();

  let totalVolume = 0n;
  const addrVolume = new Map<Address, bigint>();
  /** update this line when a comprehensive accounting address is provided */
  const zeroAddress = '0x0000000000000000000000000000000000000000';

  for (const s of sacrifices) {
    let bonus = 0n;
    let addr = s.creditAddress;
    if (addr === zeroAddress) {
      addr = comprehensiveAccountingAddress;
    }
    const ts = +s.minedTimestamp;
    const day = getSacrificeDay(ts);
    const auditDate = getAuditDate(ts);
    const date = formatAuditDate(auditDate);

    // turns the amount into a decimal, which is what price calculated set against
    const decimalFactor = BigInt(`1${'0'.repeat(s.decimals)}`);
    const scaledUsdValue = (s.amount * s.usdPrice) / decimalFactor;

    let credits = calcSacrificeCredits(priceIncreaseDay, scaledUsdValue, day, s.isSens);

    appendAudit(addrAudits, addr, {
      date,
      type: 'credit',
      value: scaledUsdValue,
      points: credits,
    });
    mapAppend(pointBreakdown, addr, {
      network: s.network,
      hash: s.transactionHash,
      currency: s.currency,
      type: 'sacrifice',
      value: credits,
    });

    switch (getBonusType(volumeCutoff, flatCutoff, day)) {
      case BonusType.VOLUME:
        if (s.creditAddress === zeroAddress) {
          break;
        }
        addPoints(addrVolume, addr, credits);
        totalVolume += credits;
        break;

      case BonusType.FLAT: {
        if (s.creditAddress === zeroAddress) {
          break;
        }
        bonus = calcFlatBonus(credits);
        appendAudit(addrAudits, addr, { date, type: 'flat_bonus', value: credits, points: bonus });
        credits += bonus;
        break;
      }

      default: // BonusType.NONE
        break;
    }
    addPoints(addrPoints, addr, credits);

    if (bonus) {
      mapAppend(pointBreakdown, addr, {
        network: s.network,
        hash: s.transactionHash,
        currency: s.currency,
        type: 'flat_bonus',
        value: bonus,
      });
    }
  }

  // Flatten map to an array, and sort by volume
  const ascendingVolumes = [...addrVolume]
    .sort((
      [_addrA, volumeA],
      [_addrB, volumeB],
    ) => (
      volumeA === volumeB
        ? 0
        : (volumeA < volumeB ? -1 : +1)
    ));

  const dateNow = formatAuditDate(new Date(0));

  let cumulativeVolumeBelow = 0n;
  for (const [addr, volume] of ascendingVolumes) {
    const bonus = calcVolumeBonus(cumulativeVolumeBelow, volume, totalVolume);

    if (addr === comprehensiveAccountingAddress) {
      continue;
    }

    appendAudit(addrAudits, addr, { date: dateNow, type: 'volume_bonus', value: volume, points: bonus });
    addPoints(addrPoints, addr, bonus);

    mapAppend(pointBreakdown, addr, {
      type: 'volume_bonus',
      value: bonus,
    });

    cumulativeVolumeBelow += volume;
  }

  if (passedArgs.project === 'pulsechain') {
    await addPulsexDust(
      addrPoints,
      pointBreakdown,
      // addrAudits,
    );
  }
  const preTotal = [...addrPoints.entries()].reduce((memo, [_address, point]) => (
    memo + point
  ), 0n);
  const noExpectationsAmount = (preTotal * 1n) / 100n;
  const ammbotAmount = (preTotal * 5n) / 100n;

  addPoints(addrPoints, noExpectationsAddress, noExpectationsAmount);
  mapAppend(pointBreakdown, noExpectationsAddress, {
    type: 'no_expectation',
    value: noExpectationsAmount,
  });
  addPoints(addrPoints, ammbotAddress, ammbotAmount);
  mapAppend(pointBreakdown, ammbotAddress, {
    type: 'ammbot',
    value: ammbotAmount,
  });
  const total = [...addrPoints.entries()].reduce((memo, [_address, point]) => (
    memo + point
  ), 0n);
  return {
    addrPoints,
    addrAudits,
    pointBreakdown,
    total,
  };
}

async function addPulsexDust(
  addrPoints: Map<string, bigint>,
  pointBreakdown: Map<string, Point[]>,
  // _addrAudits: Map<Address, AuditEntry[]>,
) {
  type Credit = {
    creditAddress: string;
    deltaHex: string;
    delta: bigint;
  };
  const csvPath = path.join(__dirname, '..', 'data', 'pulsex', 'credits.csv');
  const fileStream = fs.createReadStream(csvPath);
  const pulsexCsv: Credit[] = await new Promise((resolve, reject) => {
    Papa.parse(fileStream, {
      complete: (result) => {
        const { data, errors } = result;
        if (errors.length) {
          reject(errors);
        } else {
          const results = data.map<Credit>((row) => {
            const [creditAddress, deltaHex, delta] = row as [string, string, string];
            return {
              creditAddress,
              deltaHex,
              delta: BigInt(delta),
            };
          });
          resolve(results);
        }
      },
    });
  });
  for (let i = 0; i < pulsexCsv.length; i += 1) {
    const { creditAddress } = pulsexCsv[i];
    // if the address has no points, then give it one
    const current = addrPoints.get(creditAddress) || 0n;
    if (current !== 0n) {
      continue;
    }
    addPoints(addrPoints, creditAddress, DECIMALS_18_ONE);
    mapAppend(pointBreakdown, creditAddress, {
      type: 'dust',
      value: DECIMALS_18_ONE,
    });
  }
}

function addPoints(m: Map<Address, bigint>, addr: Address, points: bigint) {
  if (!points) {
    return;
  }
  const existingPoints = m.get(addr) || 0n;
  m.set(addr, existingPoints + points);
}
