/**
 * Construct a type with each property of T as a string type.
 */
export type PropertiesAsStrings<T> = {
  [P in keyof T]: string;
};

export type HasPrefix<V extends string, P extends string> = V extends `${P}${infer _}` ? V : never;

/** A lowercase 40 character hex address (20 bytes) with a `'0x'` prefix */
export type Address = string;

const NetworksList = [
  'arb',
  'avalanche',
  'binancechain',
  'binancesmartchain',
  'bitcoin',
  'bitcoin-cash',
  'cardano',
  'cronos',
  'dogecoin',
  'eos',
  'ethereum',
  'ethereumclassic',
  'ethereum-optimism',
  'ftm',
  'fuse',
  'litecoin',
  'monero',
  'polygon',
  'ripple',
  'sens',
  'stellar',
  'terra',
  'theta',
  'tron',
  'zcash',
  'zksync',
] as const;
export const Networks = new Set(NetworksList);

export type Network = typeof NetworksList[number];

export const SourcesList = [
  'missing',
  'ccxt/binance',
  'ccxt/binancedex',
  'ccxt/coinbasepro',
  'ccxt/ftx',
  'ccxt/hitbtc',
  'ccxt/kucoin',
  'ccxt/mexc',
  'coingecko',
  'dexs/pancakeswap',
  'dexs/pangolin',
  'dexs/quickswap',
  'dexs/uniswap',
  'dollarpegged',
  'sales/ratio',
] as const;

export const Sources = new Set(SourcesList);

export type Source = typeof SourcesList[number];

export interface SacrificeFields {
  minedTimestamp: Date;
  transactionHash: string;
  creditAddressId: number;
  isSens: boolean;
  network: Network;
  blockId: number;
  currency: string;
  ticker: string;
  decimals: number;
  source: Source;
  creditAddress: Address;
  advertisedFor: boolean;
  ignore: boolean;
  amount: bigint;
  usdPrice: bigint;
}

export type SourceRank = 0 | 1 | 2 | 3 | 4 | 5;

export interface Sacrifice extends SacrificeFields {
  sourceRank: SourceRank;
  rawRow: string;
}

export type SourceRankMap<S extends Source = Source> = Record<S, SourceRank>;

export function rankForSources<S extends Source, P extends string = ''>(
  rank: SourceRank,
  sources: readonly S[],
  prefix = '' as P,
) {
  return Object.fromEntries(
    sources
      .filter((source) => source.startsWith(prefix))
      .map((source) => [source, rank]),
  ) as SourceRankMap<HasPrefix<S, P>>;
}

export type Point = {
  network?: string;
  hash?: string;
  currency?: string;
  type: string;
  value: bigint;
};

export type SacrificeMap = Map<string, Sacrifice>;
