import * as fs from 'fs';

import { Sacrifice } from './types';
import { RAW_CSV_HEADER } from './validation';

export function exportRawCsv(rawCsvPath: string, sacrifices: Sacrifice[]) {
  let text = RAW_CSV_HEADER + '\n';

  for (const s of sacrifices) {
    text += s.rawRow + '\n';
  }

  fs.writeFileSync(rawCsvPath, text);
}
