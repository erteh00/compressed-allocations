package main

import (
	"compressed-allocations/src-go/stakers/internal/config"
	"compressed-allocations/src-go/stakers/internal/csv_saver"
	"compressed-allocations/src-go/stakers/internal/service/from_graqph"
	"compressed-allocations/src-go/utils"

	log "github.com/sirupsen/logrus"
)

func main() {
	cfg, err := config.Load()
	utils.PanicFail(err)

	graphqlClient := from_graqph.NewClient(cfg)
	totalDepositors, err := graphqlClient.GetTotalDepositorsAmount()
	utils.PanicFail(err)

	log.Infof("total depositors: %v", totalDepositors)

	res, err := graphqlClient.GetAllStakingData(totalDepositors)
	utils.PanicFail(err)

	log.Infof("Received data for %v depositors \n", len(res))

	err = csv_saver.SaveToCSV(res, config.TheGraphCsvDestination)
	utils.PanicFail(err)

	log.Infof("Finished without error, check file in %v", config.TheGraphCsvDestination)
}
