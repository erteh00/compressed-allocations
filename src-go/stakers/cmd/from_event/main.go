package main

import (
	"time"

	"compressed-allocations/src-go/stakers/generated/deposit_contract"
	"compressed-allocations/src-go/stakers/internal/config"
	"compressed-allocations/src-go/stakers/internal/csv_saver"
	"compressed-allocations/src-go/stakers/internal/service/from_event"
	"compressed-allocations/src-go/utils"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	gethRPC "github.com/ethereum/go-ethereum/rpc"
	log "github.com/sirupsen/logrus"
)

const depositContractAddress = "0x00000000219ab540356cBB839Cbe05303d7705Fa"

func main() {
	defer utils.TimeTrack(time.Now(), "Query staking events")

	cfg, err := config.Load()
	utils.PanicFail(err)

	rpc, err := gethRPC.Dial(cfg.ArchiveNode)
	utils.PanicFail(err)
	client := ethclient.NewClient(rpc)

	contractAddress := common.HexToAddress(depositContractAddress)
	contract, err := deposit_contract.NewDepositContract(contractAddress, client)
	utils.PanicFail(err)

	eventService := from_event.NewService(client, contract, cfg)
	stakers := eventService.GetAllStakers(cfg.BeginningBlockNumber, cfg.EndingBlockNumber)

	// Save result to file
	s, err := stakers.GetAsArray()
	utils.PanicFail(err)

	err = csv_saver.SaveToCSV(s, config.EventCsvDestination)
	utils.PanicFail(err)

	log.Infof("Finished without error, check file in %s", config.EventCsvDestination)
}
