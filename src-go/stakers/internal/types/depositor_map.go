package types

import (
	"errors"
	"strconv"
	"sync"
	"sync/atomic"

	"github.com/ethereum/go-ethereum/common"
)

const decimalBase = 10

type DepositorMap struct {
	m sync.Map
}

// Get Retrieves the count without modifying it
func (s *DepositorMap) Get(address common.Address) (uint64, bool) {
	staked, ok := s.m.Load(address)
	if ok {
		s, ok := staked.(*uint64)
		if ok {
			return atomic.LoadUint64(s), true
		}
	}

	return 0, false
}

func (s *DepositorMap) Add(address common.Address, value uint64) (uint64, error) {
	staked, loaded := s.m.LoadOrStore(address, &value)
	if loaded {
		v, ok := staked.(*uint64)
		if ok {
			return atomic.AddUint64(v, value), nil
		}
	}

	v, ok := staked.(*uint64)
	if ok {
		return *v, nil
	}

	return 0, errors.New("failed to add value to address")
}

func (s *DepositorMap) GetAsArray() ([]Depositor, error) {
	var depositors []Depositor
	var err error = nil

	s.m.Range(func(key, value interface{}) bool {
		address, ok := key.(common.Address)
		if !ok {
			err = errors.New("failed to make concrete type for address")

			return false
		}
		totalDeposit, ok := value.(*uint64)
		if !ok {
			err = errors.New("failed to make concrete type for total deposit")

			return false
		}

		depositors = append(depositors, Depositor{
			Id:                         address.String(),
			TotalAmountDepositedInGwei: strconv.FormatUint(*totalDeposit, decimalBase),
		})

		return true
	})

	return depositors, err
}
