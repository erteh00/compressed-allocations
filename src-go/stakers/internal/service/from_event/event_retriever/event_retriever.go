package event_retriever

import (
	"context"
	"encoding/binary"
	"time"

	"compressed-allocations/src-go/stakers/generated/deposit_contract"
	"compressed-allocations/src-go/stakers/internal/types"
	"compressed-allocations/src-go/utils"

	"github.com/avast/retry-go"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	ethTypes "github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/gammazero/workerpool"
	log "github.com/sirupsen/logrus"
)

type eventsRetriever struct {
	client   *ethclient.Client
	contract *deposit_contract.DepositContract
	stakers  *types.DepositorMap
}

// nolint: ireturn, stylecheck
func New(client *ethclient.Client, contract *deposit_contract.DepositContract) EventsRetriever {
	return &eventsRetriever{
		client:   client,
		contract: contract,
		stakers:  &types.DepositorMap{},
	}
}

func (e *eventsRetriever) BatchQueryEvents(start, end uint64, txWorkerPoolSize int) {
	log.Debugf("Launch query - filter blocks from start %d, to end %d", start, end)

	filter := bind.FilterOpts{
		Start:   start,
		End:     &end,
		Context: context.Background(),
	}

	var it *deposit_contract.DepositContractDepositEventIterator = nil
	var err error
	err = retry.Do(func() error {
		it, err = e.contract.FilterDepositEvent(&filter)
		if err != nil {
			log.Debug("FilterDepositEvent failed, cooldown.")
			time.Sleep(3 * time.Second)
		}
		return err
	})

	utils.PanicFail(err)

	//nolint: errcheck
	defer it.Close()

	wp := workerpool.New(txWorkerPoolSize)
	for it.Next() {
		work := e.prepareSingleEvent(it.Event.Raw.TxHash, binary.LittleEndian.Uint64(it.Event.Amount))
		wp.Submit(work)
	}
	wp.StopWait()
}

func (e *eventsRetriever) prepareSingleEvent(txHash common.Hash, amount uint64) func() {
	return func() {
		// Get the corresponding transaction
		var tx *ethTypes.Transaction = nil
		var err error
		err = retry.Do(func() error {
			tx, _, err = e.client.TransactionByHash(context.Background(), txHash)
			if err != nil {
				log.Debug("TransactionByHash failed, cooldown.")
				time.Sleep(3 * time.Second)
			}
			return err
		}, retry.Delay(time.Second))
		utils.PanicFail(err)

		// Get the address for refunding without additional RPC call
		// https://github.com/ethereum/go-ethereum/issues/22918
		// https://github.com/ethereum/go-ethereum/issues/23890
		asMessage, err := tx.AsMessage(ethTypes.LatestSignerForChainID(tx.ChainId()), tx.GasPrice())
		utils.PanicFail(err)

		address := asMessage.From()
		staked, err := e.stakers.Add(address, amount)
		utils.PanicFail(err)

		log.Debugf("Hash: %s, amount: %d GWEI, from: %s", txHash, staked, address)
	}
}

func (e *eventsRetriever) GetStakers() *types.DepositorMap {
	return e.stakers
}
