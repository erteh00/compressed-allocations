package audit

import (
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestCompareCSV(t *testing.T) {
	var tests = []struct {
		testname               string
		eventCsvDestination    string
		theGraphCsvDestination string
		depositorLength        int
		err                    error
	}{
		{
			testname:               "success",
			eventCsvDestination:    "./test_files/stakers-event-valid.csv",
			theGraphCsvDestination: "./test_files/stakers-graph-valid.csv",
			depositorLength:        3,
			err:                    nil,
		},
		{
			testname:            "invalid_eventCsvDestination",
			eventCsvDestination: "./invalid_eventCsvDestination",
			err:                 errors.Wrap(errors.New("failed to open csv file: open ./invalid_eventCsvDestination: no such file or directory"), failedToReadFromEventCsvDestinationErr),
		},
		{
			testname:               "invalid_theGraphCsvDestination",
			eventCsvDestination:    "./test_files/stakers-event-valid.csv",
			theGraphCsvDestination: "./invalid_theGraphCsvDestination",
			err:                    errors.Wrap(errors.New("failed to open csv file: open ./invalid_theGraphCsvDestination: no such file or directory"), failedToReadFromTheGraphCsvDestinationErr),
		},
		{
			testname:               "csv_items_count_mismatch",
			eventCsvDestination:    "./test_files/stakers-event-valid.csv",
			theGraphCsvDestination: "./test_files/stakers-graph-less-items.csv",
			err:                    errors.New(numberOfDepositorMustMatchErr),
		},
		{
			testname:               "depositors_address_dont_match",
			eventCsvDestination:    "./test_files/stakers-event-valid.csv",
			theGraphCsvDestination: "./test_files/stakers-graph-address-mismatch.csv",
			err:                    errors.New(depositorAddressesDontMatchErr),
		},
		{
			testname:               "deposited_amount_doesnt_match",
			eventCsvDestination:    "./test_files/stakers-event-valid.csv",
			theGraphCsvDestination: "./test_files/stakers-graph-deposited-amount-mismatch.csv",
			err:                    errors.New(totalDepositedAmountMismatchErr),
		},
	}

	for _, tt := range tests {
		t.Run(tt.testname, func(t *testing.T) {
			stakers, err := CompareCSV(tt.eventCsvDestination, tt.theGraphCsvDestination)
			if tt.err != nil {
				assert.Equal(t, tt.err.Error(), err.Error())
			}

			assert.Equal(t, tt.depositorLength, len(stakers))
		})
	}
}
